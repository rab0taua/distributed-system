# README #

### Description ###
One of the good solution for this task will be using microservices architecture. Two modules - order and voucher - 
can be stand-alone modules. They can communicate together through REST, Soap, message queues, or other communication 
channels (Event-driven architecture paradigm).

If you want to make a change in some module, you can change just a single component and the rest of the microservices, 
as long as the contract between them is respected, should not be impacted by that change. To make some changes you have 
to ensure the backward compatibility between microservices.

There are many technologies these days which can be used to stream events from one module to another, 
like Aws Kinesis, Apache Flink, RabbitMq and many others. In this approach small decoupled microservices (modules) can 
be developed separately, with different programming languages, by different teams and which can scale separately by 
their specific needs. You need to tie more microservices together.

The disadvantages of this approach are a complex integration testing, more remote calls, bigger security challenges, 
etc. Management of the separated modules can cost more than monolith application. 

Another approach can be using REST API technology. 

### Stack of technologies were used: ###
* Lumen
* SQLite DB
* RabbitMq
* Node.js

I use Lumen Framework for both modules - Order and Voucher. Also, I choose RabbitMq (installed locally) to organise 
communication between modules, using jobs, events and queues. For this, I use driver rabbitmq driver based on 
https://github.com/vyuldashev/laravel-queue-rabbitmq package. For storing created order and voucher I use SQLite DB, 
where also the failed jobs are stored. The data which is transferred between modules should be serialized.

The Order module is a producer of events and messages. The Voucher module is consumer of these messages. If an order is 
valued more than 100, an even is fired and a message goes to a queue. 
The Voucher module listens on this queue and consumes this message. By this event, the Voucher module creates voucher 
and stores it in DB.

The settings for RabbitMq are configured and stored in the config/queue.php and .env (see .env.example) files.
Need to run ``composer install`` command for each module, make migrations by ``php artisan migrate``.

For testing, I use Laravel command ``php artisan order:create`` to create order and fire the appropriate event in
the Order module. Then the message with needed data will be sent to the queue. 
Please, see ``order/app/Console/Commands/CreateOrder.php`` and class.
The same can be done in controller by the action of creating an order or by test.

To consume the messages, the command ``php artisan rabbitmq:consume`` should be launch in the Voucher module. 
The listener (consumer) will start its work and when the message comes to the appropriate queue it will create a 
voucher.
Please, see ``voucher/app/Listeners/OrderCreatedListener.php`` class.

The failed jobs are handled by the database and stored in failed_jobs table in database.sqlite DB.

The projects can be set by installing all needed packages and dependencies with composer. Then databases can be set by
migrations and seeds.

The Voucher module can be developed based on Node.js. Such realisation of consumer can be found in ``nodejs-voucher`` 
folder, file ``app.js`. I added three packages to connect to RabbitMq server, to connect to SQLite DB and to make
deserialize of payload from message.

Event-driven architecture allows developing the modules or microservices on different programming languages.