<?php

namespace App\Listeners;

use App\Models\Voucher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class OrderCreatedListener implements ShouldQueue
{
    public function __construct()
    {

    }

    public function handle($event)
    {
        // Create and save voucher for particular user and order.
        $voucher = new Voucher();
        $voucher->user_id = $event->payload['userId'];
        $voucher->order_id = $event->payload['orderId'];
        $voucher->save();

        Log::info('Receive order which worth more than 100 Euro: ' . json_encode($event));
    }
}
