<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Seeders\TruncateTable;
use Database\Seeders\DisableForeignKeys;

class DatabaseSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('users');
        $this->truncate('orders');

        User::factory()
            ->count(1)
            ->create();

        Order::factory()
            ->count(1)
            ->create();

        $this->enableForeignKeys();
    }
}
