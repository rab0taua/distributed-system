let amqp = require('amqplib/callback_api');
let sqlite3 = require('sqlite3').verbose();
let unserialize = require('php-serialization').unserialize;


let amqpConn = null;
let amqp_url = process.env.CLOUDAMQP_URL || 'amqp://localhost:5672';

// Connect to the RabbitMq and start to listen.
function start() {
    amqp.connect(amqp_url + "?heartbeat=60", function(err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 1000);
        }
        conn.on("error", function(err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function() {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 1000);
        });
        console.log("[AMQP] connected");
        amqpConn = conn;
        whenConnected();
    });
}

function whenConnected() {
    startWorker();
}

// A worker that acks messages only if processed successfully
function startWorker() {
    amqpConn.createChannel(function(err, ch) {
        if (closeOnErr(err)) return;
        ch.on("error", function(err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function() {
            console.log("[AMQP] channel closed");
        });

        ch.prefetch(10);
        ch.assertQueue("voucher", { durable: true }, function(err, _ok) {
            if (closeOnErr(err)) return;
            ch.consume("voucher", function (msg) {
                try {
                    // Deserialization the payload from the queue's message.
                    $data = msg.content.toString();
                    $data = JSON.parse($data);

                    $payload = $data.data.command;
                    $payload = unserialize($payload);
                    $payload = $payload.data['0'].payload;

                    if ($payload.userId && $payload.orderId) {

                        // Connect to the DB.
                        let db = new sqlite3.Database('database.sqlite', sqlite3.OPEN_READWRITE, (err) => {
                            if (err) {
                                console.error(err.message);
                            }
                            console.log('Database is connected!');
                        });

                        // Create and save voucher to the DB.
                        db.run(`INSERT INTO vouchers (user_id, order_id, value, currency) VALUES (?,?,?,?)`,
                            [$payload.userId, $payload.orderId, 5, 'Euro'], function (err) {
                                if (err) {
                                   return console.error(err.message);
                                }
                                console.log('The voucher is created and stored in DB.');
                            });

                        // Closer connection to DB.
                        db.close((err) => {
                            if (err) {
                                console.error(err.message);
                            }
                            console.log('Database is closed!');
                        });
                    }

                    ch.ack(msg);
                } catch (e) {
                    ch.reject(msg, true);
                    closeOnErr(e);
                }
            }, { noAck: false });

            console.log("Worker is started");
        });
    });
}

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

// Start consumer and listen to the queue.
start();