<?php

namespace App\Events;


class OrderCreated extends Event
{
    public $payload;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
    }
}
