<?php
namespace App\Models\Relationship;

/**
 * Class TradeOrderRelationship.
 */
trait UserRelationship
{
    /**
     * Get user' orders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Order');
    }
}
