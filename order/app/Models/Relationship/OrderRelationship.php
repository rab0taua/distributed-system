<?php
namespace App\Models\Relationship;

/**
 * Class OrderRelationship.
 */
trait OrderRelationship
{
    /**
     * Get order's user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
