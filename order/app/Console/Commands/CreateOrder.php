<?php

namespace App\Console\Commands;

use App\Models\Order;
use Faker\Factory;
use Illuminate\Console\Command;
use Faker\Generator as Faker;

class CreateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an order.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * I use this command
     *
     * @return mixed
     */
    public function handle(Order $order)
    {
        $faker = Factory::create();

        // Create new order.
        $order->user_id = 1;
        $order->product_name = $faker->regexify('[A-Za-z0-9]{20}');
        $order->quantity = rand(100,1000);
        $order->total_price = rand(10,1000);
        $order->save();

        // Apply business rule:
        //“If an order that costs more than 100 euro gets fulfilled, the customer will receive a voucher worth 5 euro“.
        if ($order->total_price > 100) {
            // Serialization.
            $payload = [
                'userId' => $order->user_id,
                'orderId' => $order->id,
                'total_price' => $order->total_price,
            ];

            event(new \App\Events\OrderCreated($payload));
        }
    }
}
